<?php
namespace CapustaSpacePlugin;

class JCapustaApi
{
    private $token;
    private $email;
    private $address = "https://api.capusta.space/v1/partner";

    public function __construct($email, $token)
    {
        $this->email = $email;
        $this->token = $token;
    }

    public static function GetDataByCurl($address, $options)
    {
        $curl = curl_init($address);

        foreach ($options as $option)
            curl_setopt($curl, $option['name'], $option['value']);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        return json_decode(curl_exec($curl));
    }

    /**
     * Gets stringified json payment info and makes query to api for
     * creating payment
     *
     * @param string $payment Json
     * @return object json
     */
    public function CreatePayment($payment)
    {
        $options = $this->GetOptions($payment);
        return self::GetDataByCurl("$this->address/payment", $options);
    }

    private function GetAuth()
    {
        return "Authorization: Bearer $this->email:$this->token";
    }

    /**
     * Made default POST options for curl query
     *
     * @param string $query Json formatted
     * @return array
     */
    private function GetOptions($query)
    {
        return [
            [
                "name" => CURLOPT_HTTPHEADER,
                "value" => [$this->GetAuth(), "Content-type: application/json"]
            ],
            [
                "name" => CURLOPT_POST,
                "value" => true
            ],
            [
                "name" => CURLOPT_POSTFIELDS,
                "value" => $query
            ]
        ];
    }
}