<?php
session_start();

if (isset($_POST) && !empty($_POST['payUrl']) && !empty($_POST['orderLink']))
    $_SESSION['orderLink'] = $_POST['orderLink'];

header("Location: " . $_POST['payUrl']);