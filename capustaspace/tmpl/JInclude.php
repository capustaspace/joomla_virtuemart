<?php
define('JOOMLA_MINIMUM_PHP', '5.3.10');

if (version_compare(PHP_VERSION, JOOMLA_MINIMUM_PHP, '<')) {
    die('Your host needs to use PHP ' . JOOMLA_MINIMUM_PHP . ' or higher to run this version of Joomla!');
}

$root = __DIR__;
$parts = explode("/", $root);

if (count($parts) <= 1)
    $parts = explode("\\", $root);

foreach ($parts as $part)
{
    $current = array_pop($parts);
    if ($current === 'plugins') break;
}

define("SITE_ROOT_DIR", implode('/', $parts));

// Saves the start time and memory usage.
$startTime = microtime(1);
$startMem = memory_get_usage();

/**
 * Constant that is checked in included files to prevent direct access.
 * define() is used in the installation folder rather than "const" to not error for PHP 5.2 and lower
 */
define('_JEXEC', 1);

if (file_exists(SITE_ROOT_DIR . '/defines.php')) {
    include_once SITE_ROOT_DIR . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', SITE_ROOT_DIR);
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_BASE . '/includes/framework.php';