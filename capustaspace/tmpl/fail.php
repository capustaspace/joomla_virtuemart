<?php
require_once __DIR__ . '/JInclude.php';
session_start();

$host = $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER["SERVER_NAME"];

$paymentId = !empty($_GET['payment_id']) ? $_GET['payment_id'] : null;

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Оплата не прошла</title>
</head>
<body>
<p>Платёж не прошёл. Для уточнения данных по заказу, обратитесь к администратору сайта.</p>
<?php if ($paymentId) : ?>
    <p>Не забудьте сообщить администратору номер вашего заказа <strong><?=$paymentId?></strong>.</p>
<?php endif; ?>
<p>Перейти <a href="<?=$host?>">обратно на сайт</a>.</p>
</body>
</html>