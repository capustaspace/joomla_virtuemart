<?php
defined ('_JEXEC') or die();
session_start();

require_once 'plugins/vmpayment/capustaspace/capustaspace/JCapustaApi.php';

$path = $_SERVER["REQUEST_SCHEME"] . "://" . $_SERVER["SERVER_NAME"] . "/plugins/vmpayment/capustaspace/capustaspace/tmpl";

$amountStr = $viewData['displayTotalInPaymentCurrency'];

$amount = 0;

preg_match('/^(?<sum>[0-9\s\,\.]*)/i', $amountStr, $result);

if (strlen($result['sum']) > 0)
    $amount = intval(str_replace(" ", "", trim(str_replace(",", ".", $result['sum'])))) * 100;

$email = $viewData['method']->cs_email;
$token = $viewData['method']->cs_token;
$projectCode = $viewData['method']->cs_projectCode;

$orderNumber = $viewData['order_number'];
$orderDescription = str_replace("{order}", $orderNumber, $viewData['method']->cs_orderDescription);

$api = new \CapustaSpacePlugin\JCapustaApi($email, $token);

$query = [
    "id" => $orderNumber,
    "amount" => [
        "amount" => $amount,
        "currency" => "RUB"
    ],
    "description" => $orderDescription,
    "projectCode" => $projectCode
];

$orderLink = JRoute::_($viewData["orderlink"], false);

$result = $api->CreatePayment(json_encode($query));

$message = "<div class='post_payment_order_total' style='width: 100%'><span class='post_payment_order_total_title'>К сожалению не удалось создать платёж. Обратитесь к администратору сайта. Не забудьте сообщить ему номер заказа</span></div>";

if ($result && $result->status === "CREATED")
    $message = <<<HTML
<form action="$path/handler.php" method="post">
    <input type="hidden" name="payUrl" value="$result->payUrl">
    <input type="hidden" name="orderLink" value="$orderLink">
    <button class="vm-button-correct">Перейти к оплате</button>
</form>
HTML;

?>
<div class="post_payment_payment_name" style="width: 100%">
	<span class="post_payment_payment_name_title"><?php echo vmText::_ ('VMPAYMENT_STANDARD_PAYMENT_INFO'); ?> </span>
	<?php echo  $viewData["payment_name"]; ?>
</div>

<div class="post_payment_order_number" style="width: 100%">
	<span class="post_payment_order_number_title"><?php echo vmText::_ ('COM_VIRTUEMART_ORDER_NUMBER'); ?> </span>
	<?php echo  $viewData["order_number"]; ?>
</div>

<div class="post_payment_order_total" style="width: 100%">
	<span class="post_payment_order_total_title"><?php echo vmText::_ ('COM_VIRTUEMART_ORDER_PRINT_TOTAL'); ?> </span>
	<?php echo  $viewData['displayTotalInPaymentCurrency']; ?>
</div>
<?=$message?>
<br><br>
<?php
if($viewData["orderlink"]){
?>
<a class="vm-button-correct" href="<?php echo JRoute::_($viewData["orderlink"], false)?>"><?php echo vmText::_('COM_VIRTUEMART_ORDER_VIEW_ORDER'); ?></a>
<?php
}
?>






