<?php
require_once __DIR__ . '/JInclude.php';

$stream = file_get_contents("php://input");
$order_data = json_decode($stream);

$db = JFactory::getDbo();

$order = new stdClass();
$order->id = !empty($order_data->transactionId) ? $order_data->transactionId : null;
$order->amount = !empty($order_data->amount->amount) ? $order_data->amount->amount / 100 : null;
$order->status = !empty($order_data->status) ? $order_data->status : null;

if (!is_null($order->status) && strtolower($order->status) === "success" && !is_null($order->id) && !is_null($order->amount))
{
    $query = $db->getQuery(true);

    $query->update($db->quoteName("#__virtuemart_orders"))
        ->set($db->quoteName("paid") . " = " . $db->quote($order->amount))
        ->where($db->quoteName("order_number") . " = " . $db->quote($order->id));

    $db->setQuery($query);
    $result = $db->execute();
}