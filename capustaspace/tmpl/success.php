<?php
require_once __DIR__ . '/JInclude.php';
session_start();

$host = $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER["SERVER_NAME"];

$paymentId = !empty($_GET['payment_id']) ? $_GET['payment_id'] : null;

$db = JFactory::getDbo();

if (!is_null($paymentId))
{
    $query = $db->getQuery(true);

    $query->select($db->quoteName("order_total"))
            ->from($db->quoteName("#__virtuemart_orders"))
            ->where($db->quoteName("order_number") . " = " . $db->quote($paymentId));

    $db->setQuery($query);
    $total = $db->loadRow()[0];

    $query = $db->getQuery(true);

    $query->update($db->quoteName("#__virtuemart_orders"))
        ->set($db->quoteName("paid") . " = " . $db->quote($total))
        ->where($db->quoteName("order_number") . " = " . $db->quote($paymentId));

    $db->setQuery($query);
    $result = $db->execute();

    if ($result) : ?>
        <!DOCTYPE html>
        <html>
        <head>
            <meta charset="utf-8">
            <title>Успешный платёж</title>
        </head>
        <body>
        <p>Платёж прошёл успешно.</p>
        <?php if (!empty($_SESSION['orderLink'])) : ?>
        <p>Сейчас вы будете перенаправлены на страницу вашего заказа.</p>
        <p>Если перенаправление не произошло автоматически, вы можете перейти в заказ <a href="<?=$_SESSION['orderLink']?>">по данной ссылке</a></p>
        <script>
            setTimeout(goToPage, 7000);

            function goToPage()
            {
                window.location.href = '<?=$_SESSION['orderLink']?>';
            }
        </script>
        <?php else : ?>
            <p>Сейчас вы будете перенаправлены обратно на сайт.</p>
            <p>Если перенаправление не произошло автоматически, вы можете перейти на сайт <a href="<?=$host?>">по данной ссылке</a></p>
            <script>
                setTimeout(goToPage, 7000);

                function goToPage()
                {
                    window.location.href = '<?=$host?>';
                }
            </script>
        <?php endif; ?>
        </body>
        </html>
    <?php endif;
}
else { ?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <title>Ошибка</title>
    </head>
    <body>
    <p>Произошла ошибка перенаправления. Информация о состоянии платежа недоступна.</p>
    <p>Сейчас вы будете перенаправлены обратно на сайт.</p>
    <p>Если перенаправление не произошло автоматически, вы можете перейти на сайт <a href="<?=$host?>">по данной ссылке</a></p>
    <script>
        setTimeout(goToPage, 7000);

        function goToPage()
        {
            window.location.href = '<?=$host?>';
        }
    </script>
    </body>
    </html>
<?php }